module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,ts,js}"
  ],
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
