# Shopping List

## Development Workflow

Develop in browser

```bash
npm run dev
```

Build static `www` directory

```bash
npm run build
```

Run on android

```bash
npm run build
npm run android
```

Test

```bash
npm run build
npm run test
```
