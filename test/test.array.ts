import { expect } from "chai";
import { insertInOrder } from "../src/trypescript/array";
import { lessThan } from "../src/trypescript/compare";

describe("array", function ()
{
  it("insertInOrder", function ()
  {
    const array = [1, 2, 4, 5];
    insertInOrder(array, 3, lessThan);

    expect(array).to.be.deep.eq([1, 2, 3, 4, 5]);
  });
});