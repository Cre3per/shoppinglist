import { expect } from "chai";
import { lessThan } from "../src/trypescript/compare";
import { MemoryList, Placement } from "../src/memoryList";
import { identity } from "../src/trypescript/functional";

describe("MemoryList", function ()
{
  it("moves forward", function ()
  {
    const memoryList = new MemoryList<number>({
      memoryDefaultOrder: lessThan,
      elementToMemoryElement: identity
    });

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.move({ from: 1, to: 2 });

    expect(memoryList.getList()).to.be.deep.eq([1, 3, 2, 4]);
  });

  it("moves backward", function ()
  {
    const memoryList = new MemoryList<number>({
      elementToMemoryElement: identity
    });

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.move({ from: 2, to: 1 });

    expect(memoryList.getList()).to.be.deep.eq([1, 3, 2, 4]);
  });

  it("moves relative backward before", function ()
  {
    const memoryList = MemoryList.withPrimitive<number>()

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.moveRelative({ from: 2, to: 1, placement: Placement.Before });

    expect(memoryList.getList()).to.be.deep.eq([1, 3, 2, 4]);
  });

  it("moves relative backward after", function ()
  {
    const memoryList = MemoryList.withPrimitive<number>()

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.moveRelative({ from: 2, to: 0, placement: Placement.After });

    expect(memoryList.getList()).to.be.deep.eq([1, 3, 2, 4]);
  });

  it("moves relative forward before", function ()
  {
    const memoryList = MemoryList.withPrimitive<number>()

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.moveRelative({ from: 1, to: 3, placement: Placement.Before });

    expect(memoryList.getList()).to.be.deep.eq([1, 3, 2, 4]);
  });

  it("moves relative forward after", function ()
  {
    const memoryList = MemoryList.withPrimitive<number>()

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.moveRelative({ from: 1, to: 2, placement: Placement.After });

    expect(memoryList.getList()).to.be.deep.eq([1, 3, 2, 4]);
  });

  it("moves relative self before", function ()
  {
    const memoryList = MemoryList.withPrimitive<number>()

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.moveRelative({ from: 1, to: 1, placement: Placement.Before });

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 3, 4]);
  });

  it("moves relative self after", function ()
  {
    const memoryList = MemoryList.withPrimitive<number>()

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);

    memoryList.moveRelative({ from: 1, to: 1, placement: Placement.After });

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 3, 4]);
  });

  it("remembers", function ()
  {
    const memoryList = new MemoryList<number>({
      memoryDefaultOrder: lessThan,
      elementToMemoryElement: identity,
    });

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);
    memoryList.push(4);
    memoryList.push(5);
    memoryList.push(6);

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 3, 4, 5, 6]);

    memoryList.move({ from: 2, to: 3 });

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 4, 3, 5, 6]);

    memoryList.splice(2, 3);

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 6]);

    memoryList.push(3);

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 3, 6]);
  });

  it("forgets", function ()
  {
    const memoryList = new MemoryList<number>({
      memoryDefaultOrder: lessThan,
      elementToMemoryElement: identity,
    });

    memoryList.push(1);
    memoryList.push(2);
    memoryList.push(3);

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 3]);

    memoryList.move({from: 1, to: 2});

    expect(memoryList.getList()).to.be.deep.eq([1, 3, 2]);

    const forgot = memoryList.forget(3);
    expect(forgot).to.be.true;

    expect(memoryList.getList()).to.be.deep.eq([1, 2]);

    memoryList.push(3);

    expect(memoryList.getList()).to.be.deep.eq([1, 2, 3]);
  });
});