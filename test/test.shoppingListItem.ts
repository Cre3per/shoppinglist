import { expect } from "chai";
import { ShoppingListItem } from "../src/shoppingListItem";

describe("ShoppingListItem", function ()
{
  it("parseFromText", function ()
  {
    {
      const parsed = ShoppingListItem.parseFromString("chips");

      expect(parsed.quantity).to.be.eq(1);
      expect(parsed.name).to.be.eq("chips");
    }
    {
      const parsed = ShoppingListItem.parseFromString("19 chips");

      expect(parsed.quantity).to.be.eq(19);
      expect(parsed.name).to.be.eq("chips");
    }
    {
      const parsed = ShoppingListItem.parseFromString("2 chicken nuggets");

      expect(parsed.quantity).to.be.eq(2);
      expect(parsed.name).to.be.eq("chicken nuggets");
    }
    {
      const parsed = ShoppingListItem.parseFromString("chicken nuggets");

      expect(parsed.quantity).to.be.eq(1);
      expect(parsed.name).to.be.eq("chicken nuggets");
    }
    {
      const parsed = ShoppingListItem.parseFromString("1911");

      expect(parsed.quantity).to.be.eq(1);
      expect(parsed.name).to.be.eq("1911");
    }
    {
      const parsed = ShoppingListItem.parseFromString("cheese 2");

      expect(parsed.quantity).to.be.eq(2);
      expect(parsed.name).to.be.eq("cheese");
    }
    {
      const parsed = ShoppingListItem.parseFromString("clay marbles 10");

      expect(parsed.quantity).to.be.eq(10);
      expect(parsed.name).to.be.eq("clay marbles");
    }
    {
      const parsed = ShoppingListItem.parseFromString("cool 6 pack");

      expect(parsed.quantity).to.be.eq(1);
      expect(parsed.name).to.be.eq("cool 6 pack");
    }
  });
});