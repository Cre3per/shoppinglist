import { expect } from "chai";
import { mapParameters, swapParameters } from "../src/trypescript/functional";

describe("functional", function ()
{
  it("swapParameters", function ()
  {
    const fn = (a: number, b: number) => [a, b];
    const swapped = swapParameters(fn);
    const out = swapped(1, 2);

    expect(out).to.be.deep.eq([2, 1]);
  });

  it("mapParameters", function ()
  {
    const add = (a: number, b: number) => a + b;
    const double = (a: number) => a * 2;

    const mapped = mapParameters(add, double);
    const out = mapped(1, 2);

    expect(out).to.be.eq(6);
  });
});