import { insertInOrder, moveElement } from "./trypescript/array";
import { equal, Equal, lessThan, LessThan } from "./trypescript/compare";
import { identity } from "./trypescript/functional";
import { Index, Integer } from "./trypescript/numeric";

function findOrInsertInOrder<T>(array: T[], element: T, eq: Equal<T>, lt: LessThan<T>): number
{
  const found = array.findIndex(el => eq(el, element));

  if (found !== -1)
  {
    return found;
  }
  else
  {
    return insertInOrder(array, element, lt);
  }
}

export enum Placement
{
  Before,
  After,
}

interface Args<Element, MemoryElement>
{
  memory?: MemoryElement[],
  memoryDefaultOrder?: LessThan<MemoryElement>,
  memoryElementEqual?: Equal<MemoryElement>,
  elementToMemoryElement: (element: Element) => MemoryElement,
}

export class MemoryList<Element, MemoryElement = Element>
{
  static withPrimitive<Element>(): MemoryList<Element, Element>
  {
    return new MemoryList<Element, Element>({
      elementToMemoryElement: identity,
    });
  }

  constructor(args: Args<Element, MemoryElement>)
  {
    const defaults = {
      memory: [],
      memoryDefaultOrder: lessThan,
      memoryElementEqual: equal,
    };
    const args_: Required<Args<Element, MemoryElement>> = { ...defaults, ...args };

    this.memory = args_.memory;
    this.memoryDefaultOrder = args_.memoryDefaultOrder;
    this.memoryElementEqual = args_.memoryElementEqual;
    this.elementToMemoryElement = args_.elementToMemoryElement;
  }

  push(value: Element): Index
  {
    findOrInsertInOrder(this.memory, this.elementToMemoryElement(value), this.memoryElementEqual, this.memoryDefaultOrder);
    return insertInOrder(this.list, value, this.tabledLessThan.bind(this));
  }

  move({ from, to }: { from: Index, to: Index })
  {
    const memoryIndexTo = this.memory.indexOf(this.elementToMemoryElement(this.list[to]));
    const memoryIndexFrom = this.memory.indexOf(this.elementToMemoryElement(this.list[from]));

    moveElement(this.memory, memoryIndexFrom, memoryIndexTo);
    moveElement(this.list, from, to);
  }

  moveRelative({ from, to, placement }: { from: Index, to: Index, placement: Placement })
  {
    if (from === to) return;

    const forward = (to > from);
    const toOffset = forward ? -1 : 0;

    switch (placement)
    {
      case Placement.Before:
        this.move({ from, to: to + toOffset });
        break;
      case Placement.After:
        this.move({ from, to: to + 1 + toOffset });
        break;
    }
  }

  splice(start: Index, deleteCount: Integer)
  {
    this.list.splice(start, deleteCount);
  }

  /**
   * If the to-be-removed element is in the list, it is removed from the list.
   */
  forget(toBeRemoved: MemoryElement): boolean
  {
    const found = this.memory.findIndex(el => this.memoryElementEqual(el, toBeRemoved));

    if (found === -1)
    {
      return false;
    }

    const foundInList = this.list.findIndex(el => this.memoryElementEqual(this.elementToMemoryElement(el), toBeRemoved));

    if (foundInList !== -1)
    {
      this.splice(foundInList, 1);
    }

    this.memory.splice(found, 1);

    return true;
  }

  get(index: Index): Element | undefined
  {
    return this.list[index];
  }

  // TODO: try making this a ReadonlyArray<Element>
  getList(): Element[]
  {
    return this.list;
  }

  getMemory(): MemoryElement[]
  {
    return this.memory;
  }

  private tabledLessThan(a: Element, b: Element)
  {
    const foundA = findOrInsertInOrder(this.memory, this.elementToMemoryElement(a), equal, lessThan);
    const foundB = findOrInsertInOrder(this.memory, this.elementToMemoryElement(b), equal, lessThan);

    return foundA < foundB;
  }

  private list: Element[] = [];
  private memory: MemoryElement[];
  private memoryDefaultOrder: LessThan<MemoryElement>;
  private memoryElementEqual: Equal<MemoryElement>;
  private elementToMemoryElement: (element: Element) => MemoryElement;
}