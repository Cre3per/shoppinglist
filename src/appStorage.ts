import { ShoppingListItem } from "./shoppingListItem";

export interface AppStorage
{
  storageVersion: number,
  items: ShoppingListItem[],
  sortingTable: string[],
}

export class AppStorage
{
  static default(): AppStorage
  {
    return {
      storageVersion: 3,
      items: [],
      sortingTable: [],
    };
  }

  static update(storage: any): AppStorage
  {
    if (storage.storageVersion == null)
    {
      storage.storageVersion = 1;
      storage.items = storage.items.map((el: any) => { return { name: el, count: 1 }; });
    }
    if (storage.storageVersion === 1)
    {
      storage.storageVersion = 2;
      storage.items = storage.items.map((el: any) => { return { name: el.name, quantity: el.count }; }) as ShoppingListItem[];
    }
    if (storage.storageVersion === 2)
    {
      storage.storageVersion = 3;
      storage.items = storage.items.map((el: any) => { return { ...el, name: el.name.trim().toLowerCase() }; }) as ShoppingListItem[];
      storage.sortingTable = storage.sortingTable.map((el: any) => el.trim().toLowerCase()) as string[];
      storage.sortingTable = [...(new Set(storage.sortingTable))];
    }

    return storage as AppStorage;
  }
}
