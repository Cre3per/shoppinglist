export class Storage
{
  static write<T>(data: T)
  {
    window.localStorage.setItem('storage', JSON.stringify(data));
  }

  static read<T>(fallback: T): T
  {
    const loaded = window.localStorage.getItem('storage');

    if (loaded)
    {
      return JSON.parse(loaded) as T;
    }

    return fallback;
  }
}