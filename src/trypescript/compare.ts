type BinaryComparison<T> = (a: T, B: T) => boolean;

export type LessThan<T> = BinaryComparison<T>;
export function lessThan<T>(a: T, b: T) { return a < b; }

export type Equal<T> = BinaryComparison<T>;
export const equal = <T>(a: T, b: T) => (a === b);