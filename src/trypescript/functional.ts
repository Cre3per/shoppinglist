export function swapParameters<A, B, Return>(fn: (a: A, b: B) => Return): (b: B, a: A) => Return
{
  return (b: B, a: A) => fn(a, b);
}

export function mapParameters<T, U, Return>
  (
    fn: (...args: T[]) => Return,
    map: (arg: U) => T
  )
  : (...args: U[]) => Return
{
  return (...args: U[]) => fn(...args.map(map));
}

export function identity<T>(value: T): T
{
  return value;
}