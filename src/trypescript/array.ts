import { LessThan } from "./compare";
import { swapParameters } from "./functional";
import { Index } from "./numeric";

export function undefinedIfMissing(index: number): Index | undefined
{
  return (index === -1) ? undefined : index;
}

export function firstLess<T>(array: T[], element: T, lt: LessThan<T>): Index | undefined
{
  const found = array.findIndex(el => lt(el, element));
  return undefinedIfMissing(found);
}

export function firstGreater<T>(array: T[], element: T, lt: LessThan<T>): Index | undefined
{
  return firstLess(array, element, swapParameters(lt));
}

export function insertInOrder<T>(array: T[], item: T, lt: LessThan<T>): Index
{
  const nextIndex = firstGreater(array, item, lt);
  const itemIndex = ((nextIndex == null) ? array.length : nextIndex);
  array.splice(itemIndex, 0, item);
  return itemIndex;
}

export function moveElement<T>(array: T[], from: Index, to: Index)
{
  const element = array[from];
  
  array.splice(from, 1);
  array.splice(to, 0, element);
}