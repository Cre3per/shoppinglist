import { Index } from "./trypescript/numeric";

export interface ShoppingListItem
{
  name: string,
  quantity: number,
}

export class ShoppingListItem
{
  static fromName(name: string): ShoppingListItem
  {
    return {
      name,
      quantity: 1,
    };
  }

  static parseFromString(text: string): ShoppingListItem
  {
    const parseIntConsume = (text: string, begin: Index, end: Index) =>
    {
      const count = parseInt(text.substring(begin, end));
      return isNaN(count) ? undefined : {
        text: text.substring(0, begin) + text.substring(end, text.length),
        count,
      };
    };

    const firstSpace = text.indexOf(' ');

    if (firstSpace !== -1)
    {
      const lastSpace = text.lastIndexOf(' ');
      const parsed = parseIntConsume(text, 0, firstSpace) || parseIntConsume(text, lastSpace, text.length);
      if (parsed != null)
      {
        return {
          name: parsed.text.trim(),
          quantity: parsed.count,
        };
      }
    }

    return ShoppingListItem.fromName(text);
  }
}